from fractions_complet import *

a = Fraction(1, 5)
b = Fraction(-3, 6)

try :
    d = Fraction(8,0)
except :
    print("Erreur lors de la création d'une fraction avec un dénominateur nul")

print(f"Essai de la simplication -3/6 = {b} ({b == Fraction(-1,2)})")

print(f"Test de la somme : 1/5 + (-3/6) = {addition(a,b)} ({addition(a,b) == Fraction(-3,10)})")

print(f"Test de la différence : 1/5 - (-3/6) = {soustraction(a,b)} ({soustraction(a,b) == Fraction(7,10)})")

print(f"Test du produit : 1/5 * (-3/6) = {multiplication(a,b)} ({multiplication(a,b) == Fraction(-1,10)})")

print(f"Test du quotient : 1/5 : (-3/6) = {division(a,b)} ({division(a,b) == Fraction(-2,5)})")

print(f"Test de l'égalité : 8/24 == (-1/-3) = {egale(Fraction(8,24),Fraction(-1,-3))}")

print(f"Test de l'égalité : 1/3 == (3333333333/10000000000) = {egale(Fraction(1,3),Fraction(3333333333,10000000000))}")
