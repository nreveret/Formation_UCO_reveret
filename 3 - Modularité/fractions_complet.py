from math import gcd

class Fraction():
    """
    Objet implémentant une fraction en python
    Chaque objet possède deux attributs : un numérateur et un dénominateur
    """

    def __init__(self, numerateur: int, denominateur: int):
        """
        Constructeur. S'utilise par exemple en tapant ma_fraction = Fraction(1,5)
        Le numérateur est un entier
        Le dénominateur est un entier non nul

        On peut récupérer le numérateur d'une fraction en faisant ma_fraction.numerateur (idem pour le dénominateur)
        """
        assert type(numerateur) == int, "Le numérateur doit être un entier"
        assert type(denominateur) == int, "Le dénominateur doit être un entier"
        assert denominateur != 0, "Le dénominateur doit être non nul"

        signe = (-1)** (numerateur * denominateur < 0)

        self.numerateur = abs(numerateur) * signe
        self.denominateur = abs(denominateur)

        self.simplification()

    def simplification(self):
        """
        Méthode simplifiant une fraction en place
        On peut l'utiliser par exemple en faisant : ma_fraction = Fraction(5,10)
        On a alors ma_fraction qui vaut 1/2
        """
        pgcd = gcd(abs(self.numerateur), self.denominateur)

        self.numerateur //= pgcd
        self.denominateur //= pgcd

    def __str__(self) -> str:
        """
        Mise en forme de l'impression de la fraction
        Fonction cachée utilisée par Python lors des print par exemple
        """

        return f"{self.numerateur}/{self.denominateur}"

    def __eq__(self, other):
        """
        Teste l'égalité de cette fraction avec une autre. Fonction cachée
        other est l'autre fraction à tester
        Retourne True si les fractions sont égales, False sinon
        """
        assert type(other) == Fraction, "other doit être une fraction"

        return self.numerateur * other.denominateur == self.denominateur * other.numerateur


def addition(frac1: Fraction, frac2: Fraction) -> Fraction:
    """
    Addition de deux fractions
    frac1 et frac2 sont deux fractions
    Retourne la somme des fractions frac1 + frac2
    """
    assert type(frac1) == Fraction, "frac1 doit être une fraction"
    assert type(frac2) == Fraction, "frac2 doit être une fraction"

    nouveau_num = frac1.numerateur * frac2.denominateur + frac1.denominateur * frac2.numerateur
    nouveau_denom = frac1.denominateur * frac2.denominateur

    return Fraction(nouveau_num, nouveau_denom)


def soustraction(frac1: Fraction, frac2: Fraction) -> Fraction:
    """
    Soustraction de deux fractions
    frac1 et frac2 sont deux fractions
    Retourne la différence des fractions frac1 - frac2
    """
    assert type(frac1) == Fraction, "frac1 doit être une fraction"
    assert type(frac2) == Fraction, "frac2 doit être une fraction"

    nouveau_num = frac1.numerateur * frac2.denominateur - frac1.denominateur * frac2.numerateur
    nouveau_denom = frac1.denominateur * frac2.denominateur

    return Fraction(nouveau_num, nouveau_denom)


def multiplication(frac1: Fraction, frac2: Fraction) -> Fraction:
    """
    Produit de deux fractions
    frac1 et frac2 sont deux fractions
    Retourne le produit des fractions frac1 * frac2
    """
    assert type(frac1) == Fraction, "frac1 doit être une fraction"
    assert type(frac2) == Fraction, "frac2 doit être une fraction"

    return Fraction(frac1.numerateur * frac2.numerateur, frac1.denominateur * frac2.denominateur)


def division(frac1: Fraction, frac2: Fraction) -> Fraction:
    """
    Quotient de deux fractions
    frac1 et frac2 sont deux fractions
    Retourne le quotient des fractions frac1 : frac2
    """
    assert type(frac1) == Fraction, "frac1 doit être une fraction"
    assert type(frac2) == Fraction, "frac2 doit être une fraction"

    return Fraction(frac1.numerateur * frac2.denominateur, frac1.denominateur * frac2.numerateur)


def egale(frac1: Fraction, frac2: Fraction) -> bool:
    """
    Teste l'égalité de deux fractions
    frac1 et frac2 sont deux fractions
    Retourne True si les fractions sont égales, False sinon
    """
    assert type(frac1) == Fraction, "frac1 doit être une fraction"
    assert type(frac2) == Fraction, "frac2 doit être une fraction"

    return frac1.numerateur * frac2.denominateur == frac1.denominateur * frac2.numerateur
