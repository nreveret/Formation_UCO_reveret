# Formation UCO - N. Revéret

Cloner ou télécharger l'ensemble du projet pour travailler en local

Cliquer sur le lien ci-dessous pour travailler en ligne sur MyBinder (pas de sauvegarde)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/nreveret%2FFormation_UCO_reveret/master)

Vous trouverez un Repl contenant les fichiers à manipuler [ici](https://repl.it/@NicolasReveret/ressourceuconr). Si vous créez un compte sur le [site](https://repl.it/), vous pourrez collaborer sur des fichiers python, html, css...